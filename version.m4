## Package version and e-mail for bugs are defined here

m4_define([GB_VERSION], [3.16.3])
m4_define([GB_MAIL], [g4mba5@gmail.com])
m4_define([GB_URL], [http://gambas.sourceforge.net])

m4_define([GB_VERSION_MAJOR], [3])
m4_define([GB_VERSION_MINOR], [16])
m4_define([GB_VERSION_RELEASE], [3])

m4_define([GB_VERSION_FULL], [0x03160003])
m4_define([GB_PCODE_VERSION], [0x03150000])
m4_define([GB_PCODE_VERSION_MIN],[0x03000000])
